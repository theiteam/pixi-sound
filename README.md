# NOTE

This is a clone of pixi-sound located at: https://github.com/pixijs/pixi-sound.
Patched to fix build errors with angular 4.

# PIXI.sound

WebAudio API playback without any Flash shims or HTML Audio fallback. Modern audio playback for modern browsers. 

**Features**

* Pausing and resuming
* Independent volume control
* Loading with XMLHttpRequest or Node's `fs` module
* Support blocking or layered sounds (multiple instances)
* Support for `PIXI.loader` system
* Dynamic filters:
    * ReverbFilter
    * DistortionFilter
    * EqualizerFilter
    * StereoFilter

**Known Compatibility**

* Chrome 58+
* Firefox 49+
* Safari 10+
* iOS 9+

### Resources

* [Releases](https://github.com/pixijs/pixi-sound/releases)
* [Examples](https://pixijs.github.io/pixi-sound/examples/index.html)
* [Sprites Example](https://pixijs.github.io/pixi-sound/examples/sprites.html)
* [App Example](https://pixijs.github.io/pixi-sound/examples/app.html)
* [API Documentation](https://pixijs.github.io/pixi-sound/docs/index.html)

## License

MIT License.
